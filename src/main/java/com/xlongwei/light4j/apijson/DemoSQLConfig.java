/*Copyright ©2016 TommyLemon(https://github.com/TommyLemon/APIJSON)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package com.xlongwei.light4j.apijson;

import apijson.framework.APIJSONSQLConfig;
import apijson.orm.AbstractParser;


/**SQL配置
 * TiDB 用法和 MySQL 一致
 * @author Lemon
 */
public class DemoSQLConfig extends APIJSONSQLConfig {

	static {
        DEFAULT_SCHEMA = "apijson";
        // DemoParser#getMaxQueryCount的方式也可以配置
        AbstractParser.DEFAULT_QUERY_COUNT = 1000;
    }

    // AbstractSQLExecutor:194生成的SQL语句的表名带引号`
    @Override
    public boolean isMySQL() {
        return true;
    }

}
